FROM ubuntu

RUN apt update && apt upgrade -y

RUN apt install openjdk-11-jdk wget -y

RUN wget https://get.jenkins.io/war-stable/2.289.2/jenkins.war

ENV JENKINS_HOME=/docker/devops-training

CMD java -jar /docker/devops-training
