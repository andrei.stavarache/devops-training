#!/bin/bash
 
echo "Acesta este un tool de replicare a functiei WC."
read -p "Va rog, introduceti pathul catre fisierul dorit: " path

check=$(find $path > /dev/null 2>&1)
if [ $? -gt 0 ] || [ -z $path ]; then
    echo "Nu ati introdus ce trebuia." ; echo "Va rog incercati!"
    exit
else
    echo  "Pentru numararea caracterelor tastati 1." ; echo "Pentru numararea cuvintelor tastati 2." ; echo "Pentru numararea randurilor apasati 3. "
fi
read actiunea

[ $actiunea = "3" ] && echo "Numarul de randuri este: "  && awk 'END{print NR}' $path
if [ $actiunea = "2" ]; then
        NUM=0
        while IFS= read -r line
        do
            for dada in $line;do
                NUM=$(( $NUM + 1 ))
            done
        done < $path
        echo "Totalul de cuvinte este $NUM"
fi
[ $actiunea = "1" ] && count=$(cat $path) && let count=${#count}+1 && echo "totalul de caractere este $count"
